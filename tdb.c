/* tdb.c
 * SLang bindings for tdb
 * 
 * Copyright (c) 2005 Paul Boekholt.
 * Released under the terms of the GNU GPL (version 2 or later).
 */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <slang.h>
#include <tdb.h>

SLANG_MODULE(tdb);

static char *Module_Version_String = "$Revision: 1.3 $";

static int TDB_Type_Id = 0;
/*{{{ TDB_Type */

typedef struct
{
   TDB_CONTEXT *p;
   int inuse;
}
TDB_Type;

static void free_tdb_type (TDB_Type *pt)
{
   if (pt->inuse)
     tdb_close(pt->p);
   SLfree ((char *) pt);
}

/*}}}*/
/*{{{ open */

static SLang_MMT_Type *allocate_tdb_type (TDB_CONTEXT *p)
{
   TDB_Type *pt;
   SLang_MMT_Type *mmt;
   pt = (TDB_Type *) SLmalloc (sizeof (TDB_Type));
   if (pt == NULL)
     return NULL;
   memset ((char *) pt, 0, sizeof (TDB_Type));

   pt->p = p;
   pt->inuse=1;
   if (NULL == (mmt = SLang_create_mmt (TDB_Type_Id, (VOID_STAR) pt)))
     {
	free_tdb_type (pt);
	return NULL;
     }   
   return mmt;
}

static int sltdb_open(char *name, int *flags, int *open_flags, int *mode)
{
   SLang_MMT_Type *mmt;
   TDB_CONTEXT *dbf;

   dbf = tdb_open(name, 0, *flags, *open_flags, *mode);
   if (dbf==NULL)
     {
	(void) SLang_push_null();
	return -1;
     }
   if (NULL == (mmt = allocate_tdb_type (dbf)))
     {
	(void) SLang_push_null();
	tdb_close(dbf);
	return -1;
     }

   if (-1 == SLang_push_mmt (mmt))
     {
	SLang_free_mmt (mmt);
	(void) SLang_push_null();
	return -1;
     }
   return 0;
}


/*}}}*/
/*{{{ close */

static void sltdb_close()
{
   TDB_Type *pt;
   SLang_MMT_Type *mmt;

   if (NULL == (mmt = SLang_pop_mmt (TDB_Type_Id)))
     return;
   pt = SLang_object_from_mmt (mmt);

   if (pt->inuse)
     {
	tdb_close(pt->p);
	pt->inuse = 0;
     }
   SLang_free_mmt (mmt);
}


/*}}}*/
/*{{{ store */

static int sltdb_store(char *key, char *value, int *flags)
{
   TDB_Type *p;
   SLang_MMT_Type *mmt;
   int ret;
   TDB_DATA k,v;
   k.dptr=key;
   k.dsize=strlen(key);
   v.dptr=value;
   v.dsize=strlen(value);

   if (NULL == (mmt = SLang_pop_mmt (TDB_Type_Id)))
     {
	SLang_free_mmt (mmt);
	return -2;
     }
   p = SLang_object_from_mmt (mmt);

   if (p->inuse)
     ret = tdb_store(p->p, k, v, *flags);
   SLang_free_mmt (mmt);
   return ret;
}

/*}}}*/
/*{{{ fetch */


static void sltdb_fetch(char *key)
{
   TDB_Type *p;
   SLang_MMT_Type *mmt;
   TDB_DATA k,v;
   char *str;
   k.dptr=key;
   k.dsize=strlen(key);

   if (NULL == (mmt = SLang_pop_mmt (TDB_Type_Id)))
     {
	SLang_free_mmt (mmt);
	(void) SLang_push_null();
	return;
     }
   
   p = SLang_object_from_mmt (mmt);

   if (p->inuse)
     {
	v = tdb_fetch(p->p, k);
	if (v.dptr == NULL 
	    || (NULL == (str = SLang_create_nslstring((char *)v.dptr, (unsigned int)v.dsize))))
	  (void)SLang_push_null();
	else
	  (void) SLang_push_string(str);
	SLfree(v.dptr);
     }
   else
     (void)SLang_push_null();
   SLang_free_mmt (mmt);
}

/*}}}*/
/*{{{ key exists */

static int sltdb_exists(char *key)
{
   TDB_Type *p;
   SLang_MMT_Type *mmt;
   TDB_DATA k;
   int ret=-1;
   k.dptr=key;
   k.dsize=strlen(key);

   if (NULL == (mmt = SLang_pop_mmt (TDB_Type_Id)))
     {
	SLang_free_mmt (mmt);
	return 0;
     }
   
   p = SLang_object_from_mmt (mmt);

   if (p->inuse)
     ret = tdb_exists(p->p, k);
   SLang_free_mmt (mmt);
   return ret;
}

/*}}}*/
/*{{{ delete key */

static int sltdb_delete(char *key)
{
   TDB_Type *p;
   SLang_MMT_Type *mmt;
   TDB_DATA k;
   int ret=-1;
   k.dptr=key;
   k.dsize=strlen(key);

   if (NULL == (mmt = SLang_pop_mmt (TDB_Type_Id)))
     {
	SLang_free_mmt (mmt);
	return 0;
     }
   
   p = SLang_object_from_mmt (mmt);

   if (p->inuse)
     ret = tdb_delete(p->p, k);
   SLang_free_mmt (mmt);
   return ret;
}

/*}}}*/
/*{{{ get keys */

/* this was adapted from stdio_fgetslines_internal 
 * According to the tdb_firstkey(3) manpage, tdb_(first|next)key works even
 * while others are writing, except in the case of nested tdb_firstkey or
 * tdb_nextkey calls, so I think this is OK
 */
static void tdb_get_keys_internal (TDB_Type *pt)
{
   unsigned int num_lines, max_num_lines;
   char **list;
   SLang_Array_Type *at;
   int inum_lines;
   TDB_DATA key, nextkey;
   
   max_num_lines = 1024;

   list = (char **) SLmalloc (sizeof (char *) * max_num_lines);
   if (list == NULL)
     return;

   num_lines = 0;
   
   key = tdb_firstkey (pt->p);

   while (key.dptr)
     {
	char *strp;
	if (max_num_lines == num_lines)
	  {
	     char **new_list;

	     max_num_lines += 4096;

	     new_list = (char **) SLrealloc ((char *)list, sizeof (char *) * max_num_lines);
	     if (new_list == NULL)
	       {
		  SLfree (key.dptr);
		  goto return_error;
	       }
	     list = new_list;
	  }
	strp = SLang_create_nslstring ((char *)key.dptr, (unsigned int) key.dsize);

	list[num_lines] = strp;
	num_lines++;
	nextkey = tdb_nextkey (pt->p, key);
	SLfree(key.dptr);
	key = nextkey;
     }

   if (num_lines != max_num_lines)
     {
	char **new_list;

	new_list = (char **)SLrealloc ((char *)list, sizeof (char *) * (num_lines + 1));
	if (new_list == NULL)
	  goto return_error;

	list = new_list;
     }

   inum_lines = (int) num_lines;
   if (NULL == (at = SLang_create_array (SLANG_STRING_TYPE, 0, (VOID_STAR) list, &inum_lines, 1)))
     goto return_error;

   if (-1 == SLang_push_array (at, 1))
     SLang_push_null ();
   return;

   return_error:
   while (num_lines > 0)
     {
	num_lines--;
	SLang_free_slstring (list[num_lines]);
     }
   SLfree ((char *)list);
   SLang_push_null ();
}

static void sltdb_get_keys()
{
   TDB_Type *pt;
   SLang_MMT_Type *mmt;
   
   if (NULL == (mmt = SLang_pop_mmt (TDB_Type_Id)))
     return;
   pt = SLang_object_from_mmt (mmt);

   if (pt->inuse)
     {
	tdb_get_keys_internal(pt);
     }
   else SLang_push_null ();
   SLang_free_mmt (mmt);
}

/*}}}*/
/*{{{ get values */

static void tdb_get_values_internal (TDB_Type *pt)
{
   unsigned int num_lines, max_num_lines;
   char **list;
   SLang_Array_Type *at;
   int inum_lines;
   TDB_DATA key, nextkey, value;
   
   max_num_lines = 1024;

   list = (char **) SLmalloc (sizeof (char *) * max_num_lines);
   if (list == NULL)
     return;

   num_lines = 0;
   
   key = tdb_firstkey (pt->p);

   while (key.dptr)
     {
	char *strp;

	value = tdb_fetch(pt->p, key);
	if (value.dptr == NULL)
	  {
	     nextkey = tdb_nextkey (pt->p, key);
	     SLfree(key.dptr);
	     key = nextkey;
	     continue;
	  }
	     
	if (max_num_lines == num_lines)
	  {
	     char **new_list;

	     max_num_lines += 4096;

	     new_list = (char **) SLrealloc ((char *)list, sizeof (char *) * max_num_lines);
	     if (new_list == NULL)
	       {
		  SLfree (key.dptr);
		  SLfree (value.dptr);
		  goto return_error;
	       }
	     list = new_list;
	  }
	strp = SLang_create_nslstring ((char *)value.dptr, (unsigned int) value.dsize);

	list[num_lines] = strp;
	num_lines++;
	nextkey = tdb_nextkey (pt->p, key);
	SLfree(key.dptr);
	SLfree(value.dptr);
	key = nextkey;
     }

   if (num_lines != max_num_lines)
     {
	char **new_list;

	new_list = (char **)SLrealloc ((char *)list, sizeof (char *) * (num_lines + 1));
	if (new_list == NULL)
	  goto return_error;

	list = new_list;
     }

   inum_lines = (int) num_lines;
   if (NULL == (at = SLang_create_array (SLANG_STRING_TYPE, 0, (VOID_STAR) list, &inum_lines, 1)))
     goto return_error;

   if (-1 == SLang_push_array (at, 1))
     SLang_push_null ();
   return;

   return_error:
   while (num_lines > 0)
     {
	num_lines--;
	SLang_free_slstring (list[num_lines]);
     }
   SLfree ((char *)list);
   SLang_push_null ();
}

static void sltdb_get_values()
{
   TDB_Type *pt;
   SLang_MMT_Type *mmt;
   
   if (NULL == (mmt = SLang_pop_mmt (TDB_Type_Id)))
     return;
   pt = SLang_object_from_mmt (mmt);

   if (pt->inuse)
     {
	tdb_get_values_internal(pt);
     }
   else SLang_push_null ();
   SLang_free_mmt (mmt);
}

/*}}}*/
/*{{{ get keys and values */
static void tdb_get_keys_values_internal (TDB_Type *pt)
{
   unsigned int num_lines, max_num_lines;
   char **list, **list2;
   SLang_Array_Type *at, *at2;
   int inum_lines;
   TDB_DATA key, nextkey, value;
   
   max_num_lines = 1024;

   list = (char **) SLmalloc (sizeof (char *) * max_num_lines);
   if (list == NULL)
     return;

   list2 = (char **) SLmalloc (sizeof (char *) * max_num_lines);
   if (list2 == NULL)
     {
	SLfree ((char *)list);
	return;
     }

   num_lines = 0;
   
   key = tdb_firstkey (pt->p);

   while (key.dptr)
     {
	char *strp, *strp2;

	value = tdb_fetch(pt->p, key);
	if (value.dptr == NULL)
	  {
	     nextkey = tdb_nextkey (pt->p, key);
	     SLfree(key.dptr);
	     key = nextkey;
	     continue;
	  }
	     
	if (max_num_lines == num_lines)
	  {
	     char **new_list, **new_list2;

	     max_num_lines += 4096;

	     new_list = (char **) SLrealloc ((char *)list, sizeof (char *) * max_num_lines);
	     if (new_list == NULL)
	       {
		  SLfree (key.dptr);
		  SLfree (value.dptr);
		  goto return_error;
	       }
	     list = new_list;
	     new_list2 = (char **) SLrealloc ((char *)list2, sizeof (char *) * max_num_lines);
	     if (new_list2 == NULL)
	       {
		  SLfree (key.dptr);
		  SLfree (value.dptr);
		  goto return_error;
	       }
	     list2 = new_list2;

	  }
	strp = SLang_create_nslstring ((char *)key.dptr, (unsigned int) key.dsize);
	strp2 = SLang_create_nslstring ((char *)value.dptr, (unsigned int) value.dsize);

	list[num_lines] = strp;
	list2[num_lines] = strp2;
	num_lines++;
	nextkey = tdb_nextkey (pt->p, key);
	SLfree(key.dptr);
	SLfree(value.dptr);
	key = nextkey;
     }

   if (num_lines != max_num_lines)
     {
	char **new_list, **new_list2;

	new_list = (char **)SLrealloc ((char *)list, sizeof (char *) * (num_lines + 1));
	if (new_list == NULL)
	  goto return_error;

	list = new_list;
	
	new_list2 = (char **)SLrealloc ((char *)list2, sizeof (char *) * (num_lines + 1));
	if (new_list2 == NULL)
	  goto return_error;

	list2 = new_list2;
     }

   inum_lines = (int) num_lines;
   if (NULL == (at = SLang_create_array (SLANG_STRING_TYPE, 0, (VOID_STAR) list, &inum_lines, 1)))
     goto return_error;
   if (NULL == (at2 = SLang_create_array (SLANG_STRING_TYPE, 0, (VOID_STAR) list2, &inum_lines, 1)))
     goto return_error;

   if (-1 == SLang_push_array (at, 1))
     SLang_push_null ();
   if (-1 == SLang_push_array (at2, 1))
     SLang_push_null ();
   return;

   return_error:
   while (num_lines > 0)
     {
	num_lines--;
	SLang_free_slstring (list[num_lines]);
	SLang_free_slstring (list2[num_lines]);
     }
   SLfree ((char *)list);
   SLfree ((char *)list2);
   SLang_push_null ();
}

static void sltdb_get_keys_and_values()
{
   TDB_Type *pt;
   SLang_MMT_Type *mmt;
   
   if (NULL == (mmt = SLang_pop_mmt (TDB_Type_Id)))
     return;
   pt = SLang_object_from_mmt (mmt);

   if (pt->inuse)
     {
	tdb_get_keys_values_internal(pt);
     }
   else 
     {
	SLang_push_null ();
	SLang_push_null ();
     }
   SLang_free_mmt (mmt);
}

/*}}}*/
/*{{{ aget/aput */
/* this is from slassoc.c */
static int pop_index (unsigned int num_indices,
		      SLang_MMT_Type **mmt,
		      TDB_Type **a,
		      char **str)
{
   if (NULL == (*mmt = SLang_pop_mmt (TDB_Type_Id)))
     {
	*a = NULL;
	*str = NULL;
	return -1;
     }

   if ((num_indices != 1)
       || (-1 == SLang_pop_slstring (str)))
     {
	SLang_verror (SL_NOT_IMPLEMENTED,
		      "TDB_Types require a single string index");
	SLang_free_mmt (*mmt);
	*mmt = NULL;
	*a = NULL;
	*str = NULL;
	return -1;
     }

   *a = (TDB_Type *) SLang_object_from_mmt (*mmt);
   return 0;
}

int _SLtdb_aget (SLtype type, unsigned int num_indices)
{
   SLang_MMT_Type *mmt;
   char *key, *val;
   TDB_Type *pt;
   int ret;
   TDB_DATA k,v;


   (void) type;

   if (-1 == pop_index (num_indices, &mmt, &pt, &key))
     return -1;

   /* get the value */
   k.dptr=key;
   k.dsize=strlen(key);

   if (pt->inuse)
     {
	v = tdb_fetch(pt->p, k);
	if (v.dptr == NULL 
	    || (NULL == (val = SLang_create_nslstring((char *)v.dptr, (unsigned int)v.dsize))))
	  (void)SLang_push_null();
	else
	  (void) SLang_push_string(val);
	SLfree(v.dptr);
     }
   else
     (void)SLang_push_null();
   
   SLang_free_slstring (key);
   SLang_free_mmt (mmt);
   return ret;
}

int _SLtdb_aput (SLtype type, unsigned int num_indices)
{
   SLang_MMT_Type *mmt;
   char *key, *val;
   TDB_Type *pt;
   int ret;
   TDB_DATA k,v;


   (void) type;

   if (-1 == pop_index (num_indices, &mmt, &pt, &key))
     return -1;
   
   if (-1 == SLpop_string (&val))
     {
	SLang_free_slstring (key);
	return -1;
     }

   ret = -1;
   
   /* put the value */
   k.dptr=key;
   k.dsize=strlen(key);

   v.dptr=val;
   v.dsize=strlen(val);
   
   /* I should probably raise an error if the tdb_store fails  */
   if (pt->inuse)
     ret = tdb_store(pt->p, k, v, TDB_REPLACE);
   
   SLang_free_slstring (key);
   SLfree (val);
   SLang_free_mmt (mmt);
   return ret;
}


/*}}}*/
/*{{{ foreach */
#if SLANG_VERSION < 20000
struct _SLang_Foreach_Context_Type
#else
struct _pSLang_Foreach_Context_Type
#endif
{
   SLang_MMT_Type *mmt;
   TDB_Type *a;
   TDB_DATA key;
#define CTX_WRITE_KEYS		1
#define CTX_WRITE_VALUES	2
   unsigned char flags;
};

static SLang_Foreach_Context_Type *
  cl_foreach_open (SLtype type, unsigned int num)
{
   SLang_Foreach_Context_Type *c;
   unsigned char flags;
   SLang_MMT_Type *mmt;

   (void) type;

   if (NULL == (mmt = SLang_pop_mmt (TDB_Type_Id)))
     return NULL;

   flags = 0;

   while (num--)
     {
	char *s;

	if (-1 == SLang_pop_slstring (&s))
	  {
	     SLang_free_mmt (mmt);
	     return NULL;
	  }

	if (0 == strcmp (s, "keys"))
	  flags |= CTX_WRITE_KEYS;
	else if (0 == strcmp (s, "values"))
	  flags |= CTX_WRITE_VALUES;
	else
	  {
	     SLang_verror (SL_NOT_IMPLEMENTED,
			   "using '%s' not supported by TDB_Type",
			   s);
	     SLang_free_slstring (s);
	     SLang_free_mmt (mmt);
	     return NULL;
	  }

	SLang_free_slstring (s);
     }

   if (NULL == (c = (SLang_Foreach_Context_Type *) SLmalloc (sizeof (SLang_Foreach_Context_Type))))
     {
	SLang_free_mmt (mmt);
	return NULL;
     }

   memset ((char *) c, 0, sizeof (SLang_Foreach_Context_Type));

   if (flags == 0) flags = CTX_WRITE_VALUES|CTX_WRITE_KEYS;

   c->flags = flags;
   c->mmt = mmt;
   c->a = (TDB_Type *) SLang_object_from_mmt (mmt);
   c->key = tdb_firstkey(c->a->p);
   return c;
}

static void cl_foreach_close (SLtype type, SLang_Foreach_Context_Type *c)
{
   (void) type;
   if (c == NULL) return;
   SLang_free_mmt (c->mmt);
   SLfree ((char *) c);
}

static int cl_foreach (SLtype type, SLang_Foreach_Context_Type *c)
{
   TDB_Type *a;
   TDB_DATA key, value;

   (void) type;

   if (c == NULL)
     return -1;

   key = c->key;
   if(key.dptr == NULL)
     return -1;
   
   a = c->a;

   if ((c->flags & CTX_WRITE_KEYS))
     {
	char *strp;
	if (NULL == (strp = SLang_create_nslstring ((char *)key.dptr, (unsigned int) key.dsize)))
	  (void)SLang_push_null();
	else
	  (void) SLang_push_string (strp);
     }

   if (c->flags & CTX_WRITE_VALUES)
     {
	char *strp2;
	value = tdb_fetch(a->p, key);
	if (value.dptr == NULL)
	  {
	     SLfree(key.dptr);
	     return -1;
	  }
	if (NULL == (strp2 = SLang_create_nslstring((char *)value.dptr, (unsigned int)value.dsize)))
	  (void)SLang_push_null();
	else
	  {
	     (void) SLang_push_string(strp2);
	  }
	SLfree(value.dptr);
     }
   c->key = tdb_nextkey (a->p, key);

   SLfree(key.dptr);

   /* keep going */
   return 1;
}

/*}}}*/
/*{{{ traverse */
static int function_marshaller(TDB_CONTEXT *tdb, TDB_DATA key, TDB_DATA dbuf, void *state)
{
   char *str;
   int ret=-1;
   SLang_Name_Type *f = (SLang_Name_Type *)state;
   if ((-1 == SLang_start_arg_list ())
       || (NULL == (str = SLang_create_nslstring((char *)key.dptr, (unsigned int)key.dsize)))
       || (-1 == SLang_push_string(str))
       || (NULL == (str = SLang_create_nslstring((char *)dbuf.dptr, (unsigned int)dbuf.dsize)))
       || (-1 == SLang_push_string(str))
       || (-1 == SLang_end_arg_list ())
       || (-1 == SLexecute_function (f)))
     return -1;
   (void) SLang_pop_integer(&ret);
   return ret;
}

static int sltdb_traverse(void)
{
   TDB_Type *p;
   SLang_MMT_Type *mmt;
   SLang_Name_Type *f=0;
   int ret = -1;

   if (SLANG_NULL_TYPE == SLang_peek_at_stack ()) 
     SLang_pop_null();
   else if (NULL == (f = SLang_pop_function ()))
     return -1;

   if (NULL == (mmt = SLang_pop_mmt (TDB_Type_Id)))
     goto end;
   
   p = SLang_object_from_mmt (mmt);

   if (!(p->inuse))
     goto end;
   
   if (f == 0)
     ret = tdb_traverse(p->p, 0, 0);
   else
     ret = tdb_traverse(p->p, &function_marshaller, (void *)f);

   end:
   SLang_free_function (f);
   SLang_free_mmt (mmt);
   return ret;
}

/*}}}*/
/*{{{ locking */
static int sltdb_lockall()
{
   TDB_Type *p;
   SLang_MMT_Type *mmt;
   int result=0;
   
   if (NULL == (mmt = SLang_pop_mmt (TDB_Type_Id)))
     goto end;
   
   p = SLang_object_from_mmt (mmt);

   if (p->inuse)
     {
	result = tdb_lockall(p->p);
     }
   end:
   SLang_free_mmt (mmt);
   return result;
}

static void sltdb_unlockall()
{
   TDB_Type *p;
   SLang_MMT_Type *mmt;
   
   if (NULL == (mmt = SLang_pop_mmt (TDB_Type_Id)))
     goto end;
   
   p = SLang_object_from_mmt (mmt);

   if (p->inuse)
     {
	tdb_unlockall(p->p);
     }
   end:
   SLang_free_mmt (mmt);
}
/*}}}*/

/*{{{ tdb error */

static char *sltdb_errorstr()
{
   TDB_Type *p;
   SLang_MMT_Type *mmt;
   char *ret="";
   if (NULL == (mmt = SLang_pop_mmt (TDB_Type_Id)))
     {
	SLang_free_mmt (mmt);
	return ret;
     }
   p = SLang_object_from_mmt (mmt);
   if (p->inuse)
     ret = (char *) tdb_errorstr(p->p);
   SLang_free_mmt(mmt);
   return ret;
}

/*}}}*/
/*{{{ destructor */

static void destroy_tdb (SLtype type, VOID_STAR f)
{
   TDB_Type *pt;
   (void) type;
   
   pt = (TDB_Type *) f;
   free_tdb_type (pt);
}

/*}}}*/
/*{{{ intrinsics */

#define DUMMY_TDB_TYPE 255
#define P DUMMY_TDB_TYPE
#define I SLANG_INT_TYPE
#define V SLANG_VOID_TYPE
#define S SLANG_STRING_TYPE

static SLang_Intrin_Fun_Type Module_Intrinsics [] =
{
   MAKE_INTRINSIC_4("tdb_open", sltdb_open, V, S, I, I, I),
/*%+
 *\function{tdb_open}
 *\synopsis{open a tdb database}
 *\usage{TDB_Type tdb_open(String_Type file, Int_Type tdb_flags, Int_Type open_flags, Int_Type mode)}
 *\description
 *   The \var{tdb_open} function opens a TDB file and returns a TDB_Type
 *   object.  On failure it returns NULL.
 *   The tdb_flags are:
 *   TDB_CLEAR_IF_FIRST - clear database if we are the only one with it open
 *   TDB_INTERNAL - don't use a file, instead store the data in
 *                  memory. The filename is ignored in this case.
 *   TDB_NOLOCK - don't do any locking
 *   TDB_NOMMAP - don't use mmap
 *   TDB_CONVERT - Create a database in the reverse of native endian:
 *                 normally when the database is created (or cleared
 *                 with TDB_CLEAR_IF_FIRST), it is created in native
 *                 endian order. This flag is set (or unset) 
 *                 automatically for existing databases.
 *   The open_flags are as in \var{open}.  
 *   The mode specifies the file permissions of a newly created file.
 *\seealso{tdb_close, tdb_store, tdb_fetch, tdb_exists, tdb_delete, tdb_errorstr}
 *%-
 */
   MAKE_INTRINSIC_0("tdb_close", sltdb_close, V),
/*%+
 *\function{tdb_close}
 *\synopsis{close a tdb database}
 *\usage{tdb_close(TDB_Type dbf)}
 *\description
 *   closes a tdb database.
 *\notes
 *   The database is also automatically closed when the \var{dbf} variable
 *   goes out of scope.
 *%-
 */
   MAKE_INTRINSIC_SSI("tdb_store", sltdb_store, I),
/*%+
 *\function{tdb_store}
 *\synopsis{stores a key/value pair in a TDB dbf}
 *\usage{Int_Type tdb_store(TDB_Type dbf, String_Type key, String_Type val, Int_Type flags)}
 *\description
 *  Inserts or replaces records in the database. If the flag is TDB_INSERT,
 *  and the key is already in the database, it fails. If the flag is 
 *  TDB_MODIFY and the key doesn't exist, it fails. On success it returns 0.
 *\notes
 *  You can also write \exmp{dbf[key] = value;}
 *\seealso{tdb_delete, tdb_fetch, tdb_exists}
 *%-
 */
   MAKE_INTRINSIC_S("tdb_fetch", sltdb_fetch, V),
/*%+
 *\function{tdb_fetch}
 *\synopsis{fetch a value from a tdb database}
 *\usage{String_Type tdb_fetch(TDB_Type dbf, String_Type key)}
 *\description
 *  Gets the value of the entry with key \var{key} from the TDB file \var{dbf}.
 *  If the key does not exist it returns NULL.
 *\notes
 *  You can also fetch the value with the expression \exmp{dbf[key]}
 *\seealso{tdb_exists}
 *%-
 */
   MAKE_INTRINSIC_S("tdb_exists", sltdb_exists, I),
/*%+
 *\function{tdb_exists}
 *\synopsis{test if a value is present in a database}
 *\usage{Integer_Type tdb_exists(TDB_Type dbf, String_Type key)}
 *\description
 *  If the \var{key} exists in the database \var{dbf}, it returns 1.
 *  Otherwise 0.
 *%-
 */
   MAKE_INTRINSIC_S("tdb_delete", sltdb_delete, I),
/*%+
 *\function{tdb_delete}
 *\synopsis{delete a record from a TDB database}
 *\usage{Integer_Type tdb_delete(TDB_Type dbf, String_Type key)}
 *\description
 *  \var{tdb_delete} removes the keyed item and the \var{key} from the database
 *   \var{dbf}.
 *  The ret value is -1 if the item is not present or the requester is a
 *  reader.  The ret value is 0 if there was a successful delete.
 *%-
 */
   MAKE_INTRINSIC_0("tdb_get_keys", sltdb_get_keys, V),
/*%+
 *\function{tdb_get_keys}
 *\synopsis{get all keys in a TDB dbf}
 *\usage{String_Type[] tdb_get_keys(TDB_Type dbf)}
 *\description
 *  This function returns all the key names of \var{dbf} as an ordinary 
 *  one dimensional array of strings.  If the \var{dbf} contains no records,
 *  an empty array will be returned.
 *\notes
 *   It is also possible to iterate over a dbf's records with \exmp{foreach
 *   (dbf) using (keys)} etc (don't do another foreach or \var{tdb_get_keys}
 *   inside the foreach loop).
 *\seealso{tdb_get_values, tdb_get_keys_and_values, tdb_traverse}
 *%-
 */
   MAKE_INTRINSIC_0("tdb_get_values", sltdb_get_values, V),
/*%+
 *\function{tdb_get_values}
 *\synopsis{get values from a tdb}
 *\usage{String_Type[] tdb_get_values(TDB_Type)}
 *\description
 *  This function returns all the values in \var{dbf} as an ordinary 
 *  one dimensional array of strings.  If the \var{dbf} contains no records,
 *  an empty array will be returned.
 *\seealso{tdb_get_keys, tdb_get_keys_and_values}
 *%-
 */
   MAKE_INTRINSIC_0("tdb_get_keys_and_values", sltdb_get_keys_and_values, V),
/*%+
 *\function{tdb_get_keys_and_values}
 *\synopsis{get keys and values from a tdb}
 *\usage{(String_Type[], String_Type[]) tdb_get_keys_and_values(TDB_Type)}
 *\description
 *  This function returns all the keys and values of \var{dbf} as two
 *  arrays of strings.  If the \var{dbf} contains no records, two empty arrays
 *  will be returned.
 *\seealso{tdb_get_keys, tdb_get_values}
 *%-
 */
   MAKE_INTRINSIC_0("tdb_errorstr", sltdb_errorstr, S),
/*%+
 *\function{tdb_errorstr}
 *\synopsis{describe the error state of a tdb database}
 *\usage{String_Type tdb_errorstr(TDB_Type)}
 *\description
 *     tdb_errorstr returns a printable string that describes the
 *     error state of the database.
 *\seealso{tdb_open}
 *%-
 */
   MAKE_INTRINSIC_0("tdb_traverse", sltdb_traverse, I),   
/*%+
 *\function{tdb_traverse}
 *\synopsis{visit every element in a tdb database}
 *\usage{String_Type tdb_traverse(TDB_Type, func)}
 *\description
 *   Apply a function to each element of an open tdb database.  The function
 *   \var{func} should have the prototype
 *#v+
 *   Integer_Type func(String_Type key, String_Type value)
 *#v-
 *    If this function call returns anything but 0, the traversal will stop.
 *    The return value is the number of elements traversed or -1 if there was
 *    an error.  Calling \var{tdb_traverse} with a NULL func parameter is the
 *    appropriate way to count the number of elements in the database.
 *\seealso{tdb_open}
 *%-
 */
   MAKE_INTRINSIC_0("tdb_lockall", sltdb_lockall, I),
/*%+
 *\function{tdb_lockall}
 *\synopsis{lock a tdb database}
 *\usage{Int tdb_lockall(TDB_Type)}
 *\description
 *   lock the database. If we already have it locked then don't do anything
 *   return 0 on success.
 *\seeals{tdb_unlockall}
 *%-
 */
   MAKE_INTRINSIC_0("tdb_unlockall", sltdb_unlockall, V),
/*%+
 *\function{tdb_unlockall}
 *\synopsis{unlock a tdb database}
 *\usage{Void tdb_unlockall(TDB_Type)}
 *\description
 *   Unlock the database.
 *\seealso{tdb_lockall}
 *%-
 */
   SLANG_END_INTRIN_FUN_TABLE
};

static SLang_Intrin_Var_Type Module_Variables [] =
{
   MAKE_VARIABLE("_tdb_module_version_string", &Module_Version_String, SLANG_STRING_TYPE, 1),
   SLANG_END_INTRIN_VAR_TABLE
};

static SLang_IConstant_Type Module_Constants [] =
{
MAKE_ICONSTANT("TDB_REPLACE", TDB_REPLACE),
MAKE_ICONSTANT("TDB_INSERT", TDB_INSERT),
MAKE_ICONSTANT("TDB_MODIFY", TDB_MODIFY),
MAKE_ICONSTANT("TDB_DEFAULT", TDB_DEFAULT),
MAKE_ICONSTANT("TDB_CLEAR_IF_FIRST", TDB_CLEAR_IF_FIRST),
MAKE_ICONSTANT("TDB_INTERNAL", TDB_INTERNAL),
MAKE_ICONSTANT("TDB_NOLOCK", TDB_NOLOCK),
MAKE_ICONSTANT("TDB_NOMMAP", TDB_NOMMAP),
MAKE_ICONSTANT("TDB_CONVERT", TDB_CONVERT),
   SLANG_END_ICONST_TABLE
};


#undef P
#undef I
#undef V
#undef S

/*}}}*/
/*{{{ register class */

static void patchup_intrinsic_table (SLang_Intrin_Fun_Type *table, 
				     unsigned char dummy, unsigned char type)
{
   while (table->name != NULL)
     {
	unsigned int i, nargs;
	SLtype *args;
	
	nargs = table->num_args;
	args = table->arg_types;
	for (i = 0; i < nargs; i++)
	  {
	     if (args[i] == dummy)
	       args[i] = type;
	  }
	
	/* For completeness */
	if (table->return_type == dummy)
	  table->return_type = type;

	table++;
     }
}


static int register_tdb_type (void)
{
   SLang_Class_Type *cl;
   
   if (TDB_Type_Id != 0)
     return 0;

   if (NULL == (cl = SLclass_allocate_class ("TDB_Type")))
     return -1;
   if ((-1 == SLclass_set_destroy_function (cl, destroy_tdb))
       || (-1 == SLclass_set_aget_function (cl, _SLtdb_aget))
       || (-1 == SLclass_set_aput_function (cl, _SLtdb_aput)))
     return -1;
#if SLANG_VERSION < 20000
   cl->cl_foreach_open = cl_foreach_open;
   cl->cl_foreach_close = cl_foreach_close;
   cl->cl_foreach = cl_foreach;
#else
   if (-1 == SLclass_set_foreach_functions(cl, cl_foreach_open, cl_foreach, cl_foreach_close))
     return -1;
#endif
   /* By registering as SLANG_VOID_TYPE, slang will dynamically allocate a
    * type.
    */
   if (-1 == SLclass_register_class (cl, SLANG_VOID_TYPE, sizeof (TDB_Type), SLANG_CLASS_TYPE_MMT))
     return -1;

   TDB_Type_Id = SLclass_get_class_id (cl);
   patchup_intrinsic_table (Module_Intrinsics, DUMMY_TDB_TYPE, TDB_Type_Id);
   return 0;
}

/*}}}*/
/*{{{ init module */


int init_tdb_module_ns (char *ns_name)
{
   SLang_NameSpace_Type *ns = SLns_create_namespace (ns_name);
   if (ns == NULL)
     return -1;
   if (-1 == register_tdb_type ())
     return -1;

   if ((-1 == SLns_add_intrin_fun_table (ns, Module_Intrinsics, "__TDB__"))
       || (-1 == SLns_add_intrin_var_table (ns, Module_Variables, NULL))
       || (-1 == SLns_add_iconstant_table (ns, Module_Constants, NULL)))
     return -1;
   
   return 0;
}

/*}}}*/
