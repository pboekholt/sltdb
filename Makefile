CC_SHARED = gcc -g $(CFLAGS) -shared -fPIC
SLANG_INC = -I/usr/local/include
SLANG_LIB = -L/usr/local/lib -lslang

LIBS = $(SLANG_LIB) -lm -ltdb
INCS = $(SLANG_INC)

all: tdb-module.so

tdb-module.so: tdb.c
	$(CC_SHARED) $(INCS) tdb.c -o tdb-module.so $(LIBS)

tdb.o: tdb.c
	gcc $(CFLAGS) $(INCS) -O2 -c -g tdb.c -o tdb.o
        
clean:
	rm -f tdb-module.so *.o
